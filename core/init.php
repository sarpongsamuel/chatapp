<?php
include 'database/connection.php';
include 'classes/user.php';
include 'classes/follow.php';
include 'classes/tweet.php';

global $pdo;

// creating objects of each classes so that they can access pdo in connection.php
$getFromU = new User($pdo);
$getFromF = new Follow($pdo);
$getFromT = new Tweet($pdo);

// our session
session_start();

// our constraint
define("BASE_URL","http://localhost:8080/atom/mini-project/twitter/");


?>
