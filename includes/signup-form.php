<?php
if(isset($_POST['signup'])){
  $screenName =$_POST['screenName'];
  $email =$_POST['email'];
  $password =$_POST['password'];
  $error ='';

  if(empty($screenName) or empty($password) or empty($email)){
    $error ="all fields are required";
  }else{
    $email      =$getFromU->checkInput($email);
    $screenName =$getFromU->checkInput($screenName);
    $password   =$getFromU->checkInput($password);

    if(!filter_var($email)){
      $error ="incorrect email format";
    }else if(strlen($screenName) > 20) {
      $error ="Name must be between 6 and 20 charaters";
    }
    else if(strlen($password) < 5) {
      $error ="password too short";
    }
    else {
      if($getFromU->checkEmail($email) === true){
        $error ="email already in use";
      }else {
        $user_id =$getFromU->create('users', array('username'=>$screenName, 'email'=>$email,'password'=>md5($password), 'screenName'=>$screenName, 'profileImage'=>'assets/images/defaultprofileimage.png','profileCover'=>'assets/images/defaultCoverImage.png'));
        $_SESSION['user_id'] = $user_id;
        header('Location: includes/signup.php?step=1');
      }
    }
  }
}


 ?>
<form method="post">
<div class="signup-div">
	<h3>Sign up </h3>
	<ul>
		<li>
		    <input type="text" name="screenName" placeholder="Full Name"/>
		</li>
		<li>
		    <input type="email" name="email" placeholder="Email"/>
		</li>
		<li>
			<input type="password" name="password" placeholder="Password"/>
		</li>
		<li>
			<input type="submit" name="signup" Value="Signup for Twitter" >
		</li>
	</ul>

  <?php if(isset($error))
   echo '<li class="error-li">
    <div class="span-fp-error">'.$error.'</div>
   </li>'
  ?>
</div>
</form>
